# CI cut

CI cut is used to cut your environment URL based on a reference slug, (in gitlab `CI_COMMIT_REF_SLUG` environment variable).

## Use it

Go to `https://gnuk.gitlab.io/cicut/?id=ID&prefix=PREFIX&suffix=SUFFIX` and replace:

* PREFIX by the prefix of your URL between protocol (https://) and the rest of your url
* SUFFIX by the suffix of your URL, this may be the end of your domain
* ID by the unique id you want to cut, for example `42-long-id-for-my-feature` will become `42-long`

> As an example, [https://gnuk.gitlab.io/cicut/?id=42-answer-to-life&prefix=www.qwant.com/?q=&suffix=%26t=web](https://gnuk.gitlab.io/cicut/?id=42-answer-to-life&prefix=www.qwant.com/?q=&suffix=%26t=web) will go to [https://www.qwant.com/?q=42-answer&t=web](https://www.qwant.com/?q=42-answer&t=web) url.

### For your CI

Create the following anchor in your .gitlab-cy.yml

```yaml
.git_slug_short: &git_slug_short |
  export GIT_SLUG_SHORT="$(echo ${CI_COMMIT_REF_SLUG} | cut -d'-' -f1,2)"
```

Then use it in your deploy `before_script`

```yaml
deploy:
  stage: deploy
  before_script:
    - *git_slug_short
  script:
    - # Your stuff, for example deploy url may be "${CI_PROJECT_NAME}-${GIT_SLUG_SHORT}.domain.ext"
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://gnuk.gitlab.io/cicut/?id=${CI_COMMIT_REF_SLUG}&prefix=${CI_PROJECT_NAME}-&suffix=.domain.ext
```

## Install it in your own CI

For a giltab project, just get all sources of this repository and push it into your own repository, the CI is ready to use for [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/).
