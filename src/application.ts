import { url } from './url';
import { cutter } from './cutter';

const searchOrEmpty = (searchParams: URLSearchParams) => (key: string) => {
  const value = searchParams.get(key);
  return value === null ? '' : value;
};

export const redirect = (windowContext: Window): void => {
  const {search} = windowContext.location;
  const searchParams = new URLSearchParams(search);
  const params = searchOrEmpty(searchParams);
  const id = params('id');
  const prefix = params('prefix');
  const suffix = params('suffix');
  const unsafe = searchParams.has('unsafe');
  windowContext.location.assign(url(cutter(id, prefix, suffix), unsafe))
};
