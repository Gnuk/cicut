import { cutter } from './cutter';

describe('Cutter', () => {
  it('Should be empty when nothing to cut', () => {
    expect(cutter()).toBe('');
  });

  it('Should be the exact value with one part', () => {
    expect(cutter('master')).toBe('master');
  });

  it('Should be the exact value with two part', () => {
    expect(cutter('1-one')).toBe('1-one');
  });

  it('Should be the two first values with more than two parts', () => {
    expect(cutter('1-long-issue-with-more-text')).toBe('1-long');
  });

  it('Should manage prefix', () => {
    expect(cutter('1-long-issue-with-more-text', 'app-')).toBe('app-1-long');
  });

  it('Should manage suffix', () => {
    expect(cutter('1-long-issue-with-more-text', undefined, '.domain.ext')).toBe('1-long.domain.ext');
  });
});
