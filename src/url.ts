export const url = (domain: string = '', unsafe: boolean = false) => {
  const protocol = unsafe ? 'http' : 'https';
  return `${protocol}://${domain}`;
};
