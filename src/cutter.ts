const extractId = (id: string): string => {
  if (!id) {
    return '';
  }
  const list = id.split('-');
  if(list.length <= 2) {
    return id;
  }
  const [first, second] = list;
  return `${first}-${second}`;
};

export const cutter = (id?: string, prefix: string = '', suffix:string = ''): string => `${prefix}${extractId(id)}${suffix}`;
