import { url } from './url';

describe('URL', () => {
  it('Should be https:// by default', () => {
    expect(url()).toBe('https://')
  });

  it('Should be contains the domain when specified', () => {
    expect(url('domain')).toBe('https://domain')
  });

  it('Should be http instead of https when unsafe', () => {
    expect(url('domain', true)).toBe('http://domain');
  });
});
