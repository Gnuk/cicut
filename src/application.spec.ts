import { redirect } from './application';
import * as sinon from 'sinon';

describe('Application', () => {
  it('Should redirect to empty domain', () => {
    const assign = sinon.stub();
    const windowContext = {
      location: {
        assign,
        search: '',
      }
    };
    redirect(windowContext as any);
    expect(assign.getCall(0).args[0]).toBe('https://');
  });

  it('Should redirect to id', () => {
    const assign = sinon.stub();
    const windowContext = {
      location: {
        assign,
        search: '?id=42-the-answer',
      }
    };
    redirect(windowContext as any);
    expect(assign.getCall(0).args[0]).toBe('https://42-the');
  });

  it('Should redirect to id with prefix and suffix', () => {
    const assign = sinon.stub();
    const windowContext = {
      location: {
        assign,
        search: '?id=42-the-answer&prefix=app-&suffix=.domain.ext',
      }
    };
    redirect(windowContext as any);
    expect(assign.getCall(0).args[0]).toBe('https://app-42-the.domain.ext');
  });

  it('Should redirect to id with prefix and suffix', () => {
    const assign = sinon.stub();
    const windowContext = {
      location: {
        assign,
        search: '?id=42-the-answer&prefix=app-&suffix=.domain.ext&unsafe',
      }
    };
    redirect(windowContext as any);
    expect(assign.getCall(0).args[0]).toBe('http://app-42-the.domain.ext');
  });
});
